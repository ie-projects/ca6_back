package org.fn.ie.domain;

public class OrderStatus {
    private int status;
    private String restaurant;

    public OrderStatus(int status, String restaurant) {
        this.status = status;
        this.restaurant = restaurant;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }
}
