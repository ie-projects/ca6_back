package org.fn.ie.repository;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.fn.ie.domain.*;

import java.sql.*;
import java.util.ArrayList;

public class NikiveryRepository {
    private static NikiveryRepository instance;

    ComboPooledDataSource dataSource;

    private NikiveryRepository() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        dataSource = new ComboPooledDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/loghme?useSSL=false");
        dataSource.setUser("root");
        dataSource.setPassword("13771375");

        dataSource.setInitialPoolSize(5);
        dataSource.setMinPoolSize(5);
        dataSource.setAcquireIncrement(5);
        dataSource.setMaxPoolSize(20);
        dataSource.setMaxStatements(100);
    }

    public static NikiveryRepository getInstance() {
        if (instance == null)
            instance = new NikiveryRepository();
        return instance;
    }

    public ArrayList<RestaurantDAO> getRestaurants(int from, int offset) throws SQLException {
        ArrayList<RestaurantDAO> restaurants = new ArrayList<RestaurantDAO>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select * from restaurant limit " + from + " , " + offset + " ;");
        while(result.next()) {
            RestaurantDAO restaurantDAO = new RestaurantDAO();
            restaurantDAO.setId(result.getString("id"));
            restaurantDAO.setName(result.getString("name"));
            Location resLoc = new Location();
            resLoc.setX(result.getInt("locx"));
            resLoc.setY(result.getInt("locy"));
            restaurantDAO.setLocation(resLoc);
            restaurantDAO.setLogo(result.getString("logo"));
            restaurants.add(restaurantDAO);
        }
        result.close();
        statement.close();
        connection.close();
        return restaurants;
    }

    public RestaurantDAO getRestaurant(String resId) throws SQLException {
        RestaurantDAO restaurantDAO = new RestaurantDAO();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select R.* from restaurant R where R.id = \"" + resId + "\" ;");
        if(result.next()) {
            restaurantDAO.setId(result.getString("id"));
            restaurantDAO.setName(result.getString("name"));
            Location resLoc = new Location();
            resLoc.setX(result.getInt("locx"));
            resLoc.setY(result.getInt("locy"));
            restaurantDAO.setLocation(resLoc);
            restaurantDAO.setLogo(result.getString("logo"));
        }
        result.close();
        statement.close();
        connection.close();
        return restaurantDAO;
    }

    public ArrayList<FoodDAO> getRestaurantMenu(String resId) throws SQLException {
        ArrayList<FoodDAO> menu = new ArrayList<FoodDAO>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select * from food F " +
                        "where F.resId = \"" + resId + "\";");
        while(result.next()) {
            FoodDAO foodDAO = new FoodDAO();
            foodDAO.setName(result.getString("name"));
            foodDAO.setDescription(result.getString("description"));
            foodDAO.setPopularity(result.getFloat("popularity"));
            foodDAO.setPrice(result.getInt("price"));
            foodDAO.setImage(result.getString("image"));
            foodDAO.setRestaurantName(result.getString("resName"));
            menu.add(foodDAO);
        }
        result.close();
        statement.close();
        connection.close();
        return menu;
    }

    public ArrayList<PartyFoodDAO> getPartyFood(String resId) throws SQLException {
        ArrayList<PartyFoodDAO> menu = new ArrayList<PartyFoodDAO>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        Statement statement2 = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select * from partyfood F ;");
        while(result.next()) {
            PartyFoodDAO partyFoodDAO = new PartyFoodDAO();
            partyFoodDAO.setCount(result.getInt("count"));
            partyFoodDAO.setOldPrice(result.getInt("oldPrice"));
            partyFoodDAO.setName(result.getString("name"));
            partyFoodDAO.setDescription(result.getString("description"));
            partyFoodDAO.setPopularity(result.getFloat("popularity"));
            partyFoodDAO.setPrice(result.getInt("price"));
            partyFoodDAO.setImage(result.getString("image"));
            partyFoodDAO.setRestaurantId(result.getString("resId"));
            ResultSet restaurantResult = statement2.executeQuery(
                    "select * from restaurant R where R.id = \"" + partyFoodDAO.getRestaurantId() + "\";");
            if(restaurantResult.next()) {
                partyFoodDAO.setRestaurantName(restaurantResult.getString("name"));
            }
            menu.add(partyFoodDAO);
            restaurantResult.close();
        }
        result.close();
        statement.close();
        connection.close();
        return menu;
    }

    public UserProfileDAO getUserProfile(int id) throws SQLException {
        UserProfileDAO userProfileDAO = new UserProfileDAO();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("select * from userprofile U where U.id = " + id + " ;");
        if(result.next()) {
            userProfileDAO.setId(result.getInt("id"));
            userProfileDAO.setFirstName(result.getString("firstname"));
            userProfileDAO.setLastName(result.getString("lastname"));
            userProfileDAO.setPhoneNumber(result.getString("phonenumber"));
            userProfileDAO.setEmail(result.getString("email"));
            userProfileDAO.setCredit(result.getInt("credit"));
            userProfileDAO.setCartRestaurant(result.getString("cartrestaurant"));
        }
        result.close();
        statement.close();
        connection.close();
        return userProfileDAO;
    }

    public FactorDAO getFactor(int userId, int factorId) throws SQLException {
        FactorDAO factor = new FactorDAO();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        Statement statement2 = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select * from factor F where F.userId = " + userId + " and F.id = " + factorId + " ;");
        ArrayList<Order> orders = new ArrayList<Order>();
        if(result.next()) {
            factor.setRestaurantName(result.getString("resName"));
            factor.setNetPrice(result.getInt("netPrice"));
        }
        result.close();
        ResultSet itemsResult = statement.executeQuery("select * from factoritem I where I.userId = " + userId +
                " and I.id = " + factorId + " ;");
        while(itemsResult.next()) {
            Order order = null;
            ResultSet priceResult = statement2.executeQuery(
                    "select * from food FO where FO.resId = \"" + itemsResult.getString("resId") +
                            "\" and FO.name = \"" + itemsResult.getString("foodName") + "\";");
            while(priceResult.next()) {
                order = new Order(itemsResult.getString("foodName"), itemsResult.getInt("count"),
                        priceResult.getInt("price"));
            }
            priceResult.close();
            if(order != null)
                orders.add(order);
        }
        itemsResult.close();
        factor.setOrders(orders);
        statement.close();
        statement2.close();
        connection.close();
        return factor;
    }

    public OrderStatus getOrderStatus(int userId, int id) throws SQLException {
        OrderStatus orderStatus = null;
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select * from orderstatus U where U.userId = " + userId +
                " and U.id = " + id + ";");
        if(result.next()) {
            orderStatus = new OrderStatus(result.getInt("status"), result.getString("resName"));
        }
        result.close();
        statement.close();
        connection.close();
        return orderStatus;
    }

    public ArrayList<FoodDAO> getCart(int userId) throws SQLException {
        ArrayList<FoodDAO> cart = new ArrayList<FoodDAO>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        Statement statement2 = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select * from cartitem C where C.userId = " + userId + " ;");
        while(result.next()) {
            ResultSet FoodResult = statement2.executeQuery(
                    "select * from food F where F.resId = \"" + result.getString("resId") +
                            "\" and F.name = \"" + result.getString("foodName") + "\";");
            if(FoodResult.next()) {
                FoodDAO foodDAO = new FoodDAO();
                foodDAO.setName(result.getString("foodName"));
                foodDAO.setDescription(FoodResult.getString("description"));
                foodDAO.setPopularity(FoodResult.getFloat("popularity"));
                foodDAO.setPrice(result.getInt("price"));
                foodDAO.setImage(FoodResult.getString("image"));
                foodDAO.setRestaurantName(FoodResult.getString("resName"));
                cart.add(foodDAO);
            }
            FoodResult.close();
        }
        result.close();
        statement.close();
        connection.close();
        return cart;
    }

    public ArrayList<String> getPartyResIds() throws SQLException {
        ArrayList<String> partyResIds = new ArrayList<String>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("select distinct F.resId from partyfood F;");
        while(result.next()) {
            partyResIds.add(result.getString("resId"));
        }
        result.close();
        statement.close();
        connection.close();
        return partyResIds;
    }

    public int getNumOfFactors(int userId) throws SQLException {
        int count = 0;
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("select count(*) as cnt from factor F where F.userId = " + userId + " ;");
        if(result.next()) {
            count = result.getInt("cnt");
        }
        result.close();
        statement.close();
        connection.close();
        return count;
    }

    public void addRestaurant(Restaurant restaurant) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert ignore into restaurant (id, name, locx, locy, logo) values (?, ?, ?, ?, ?);");
            pStatement.setString(1, restaurant.getId());
            pStatement.setString(2, restaurant.getName());
            pStatement.setInt(3, restaurant.getLocation().getX());
            pStatement.setInt(4, restaurant.getLocation().getY());
            pStatement.setString(5, restaurant.getLogo());
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addFood(String resId, String resName, Food food) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert ignore into food (resId, name, description, popularity, price, image, resName) values (?, ?, ?, ?, ?, ?, ?);");
            pStatement.setString(1, resId);
            pStatement.setString(2, food.getName());
            pStatement.setString(3, food.getDescription());
            pStatement.setFloat(4, food.getPopularity());
            pStatement.setInt(5, food.getPrice());
            pStatement.setString(6, food.getImage());
            pStatement.setString(7, resName);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPartyFood(String resId, PartyFood food) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert ignore into partyfood (resId, name, description, popularity, price, image, count, oldPrice) " +
                            "values (?, ?, ?, ?, ?, ?, ?, ?);");
            pStatement.setString(1, resId);
            pStatement.setString(2, food.getName());
            pStatement.setString(3, food.getDescription());
            pStatement.setFloat(4, food.getPopularity());
            pStatement.setInt(5, food.getPrice());
            pStatement.setString(6, food.getImage());
            pStatement.setInt(7, food.getCount());
            pStatement.setInt(8, food.getOldPrice());
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addFactor(int userId, int id, String resName, int netPrice) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into factor (userId, id, resName, netPrice) " +
                            "values (?, ?, ?, ?);");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.setString(3, resName);
            pStatement.setInt(4, netPrice);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addFactorItem(int userId, int id, String resId, String foodName, int count) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into factoritem (userId, id, resId, foodName, count) " +
                            "values (?, ?, ?, ?, ?);");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.setString(3, resId);
            pStatement.setString(4, foodName);
            pStatement.setInt(5, count);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addOrderStatus(int userId, int id, int status, String resName) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into orderstatus (userId, id, status, resName) " +
                            "values (?, ?, ?, ?);");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.setInt(3, status);
            pStatement.setString(4, resName);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addCartItem(int userId, int id, String resId, String foodName, int price) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into cartitem (userId, id, resId, foodName, price) " +
                            "values (?, ?, ?, ?, ?);");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.setString(3, resId);
            pStatement.setString(4, foodName);
            pStatement.setInt(5, price);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setUserCartRestaurant(int userId, String resId) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "update userprofile set cartrestaurant = ? where id = ? ;");
            pStatement.setString(1, resId);
            pStatement.setInt(2, userId);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean doesRestaurantExist(String resId) throws SQLException {
        boolean exists = false;
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select count(*) as cnt from restaurant R where R.id = \"" + resId + "\";");
        if(result.next()) {
            exists = !(result.getInt("cnt") == 0);
        }
        result.close();
        statement.close();
        connection.close();
        return exists;
    }

    public boolean doesFoodExist(String resId, String foodName) throws SQLException {
        boolean exists = false;
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select count(*) as cnt from food F where F.resId = \"" + resId +
                "\" and F.name = \"" + foodName + "\";");
        if(result.next()) {
            exists = !(result.getInt("cnt") == 0);
        }
        result.close();
        statement.close();
        connection.close();
        return exists;
    }

    public boolean doesPartyFoodExist(String resId, String foodName) throws SQLException {
        boolean exists = false;
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select count(*) as cnt from partyfood F where F.resId = \"" + resId +
                        "\" and F.name = \"" + foodName + "\";");
        if(result.next()) {
            exists = !(result.getInt("cnt") == 0);
        }
        result.close();
        statement.close();
        connection.close();
        return exists;
    }

    public void removeCartItems(int userId) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "delete from cartitem where userId = ? ;");
            pStatement.setInt(1, userId);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeCartItem(int userId, int id) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "delete from cartitem where userId = ? and id = ? ;");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void decreasePartyFoodCount(String foodName, String cartRestaurant, int count) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(
                    "select * from partyfood F where F.resId = \"" + cartRestaurant +
                            "\" and F.name = \"" + foodName + "\";");
            int initialCount = 0;
            if(result.next()) {
                initialCount = result.getInt("count");
            }
            result.close();
            statement.close();
            PreparedStatement pStatement = connection.prepareStatement(
                    "update partyfood set count = ? where resId = ? and name = ? ;");
            pStatement.setInt(1, initialCount - count);
            pStatement.setString(2, cartRestaurant);
            pStatement.setString(3, foodName);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setDeliveryState(int status, int orderId, int userId) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "update orderstatus set status = ? where userId = ? and id = ? ;");
            pStatement.setInt(1, status);
            pStatement.setInt(2, userId);
            pStatement.setInt(3, orderId);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setCredit(int credit, int userId) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "update userprofile set credit = ? where id = ? ;");
            pStatement.setInt(1, credit);
            pStatement.setInt(2, userId);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeFoodParty() {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "delete from partyfood ;");
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void batchUpdateRestaurants(ArrayList<Restaurant> restaurants) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            connection.setAutoCommit(false);
            for(int i = 0; i < restaurants.size(); i++) {
                String resAdd = "insert ignore into restaurant (id, name, locx, locy, logo) values (\"" +
                        restaurants.get(i).getId() + "\", \"" +
                        restaurants.get(i).getName() + "\", " +
                        restaurants.get(i).getLocation().getX() + ", " +
                        restaurants.get(i).getLocation().getY() + ", \"" +
                        restaurants.get(i).getLogo() + "\");";
                statement.addBatch(resAdd);
                ArrayList<Food> menu = restaurants.get(i).getMenu();
                if(menu != null)
                    for(int j = 0; j < menu.size(); j++) {
                        String foodAdd = "insert ignore into food (resId, name, description, popularity, price, image, resName)" +
                                " values (\"" +
                                restaurants.get(i).getId() + "\", \"" +
                                menu.get(j).getName() + "\", \"" +
                                menu.get(j).getDescription() + "\", " +
                                menu.get(j).getPopularity() + ", " +
                                menu.get(j).getPrice() + ", \"" +
                                menu.get(j).getImage() + "\", \"" +
                                restaurants.get(i).getName() + "\");";
                        statement.addBatch(foodAdd);
                    }
                ArrayList<PartyFood> partyMenu = restaurants.get(i).getPartyMenu();
                if(partyMenu != null)
                    for(int j = 0; j < partyMenu.size(); j++) {
                        NikiveryRepository.getInstance().addPartyFood(restaurants.get(i).getId(), partyMenu.get(j));
                        String partyAdd = "insert ignore into partyfood (resId, name, description, popularity, price, image, count, oldPrice)" +
                                " values (\"" +
                                restaurants.get(i).getId() + "\", \"" +
                                partyMenu.get(j).getName() + "\", \"" +
                                partyMenu.get(j).getDescription() + "\", " +
                                partyMenu.get(j).getPopularity() + ", " +
                                partyMenu.get(j).getPrice() + ", \"" +
                                partyMenu.get(j).getImage() + "\", " +
                                partyMenu.get(j).getCount() + ", " +
                                partyMenu.get(j).getOldPrice() + ");";
                        statement.addBatch(partyAdd);
                    }
            }
            int[] results = statement.executeBatch();
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<RestaurantDAO> getSearchRestaurants(String foodName, String resName, int from, int offset) throws SQLException {
        ArrayList<RestaurantDAO> restaurants = new ArrayList<RestaurantDAO>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select R.* from restaurant R, food F where R.name like '%" + resName + "%' and F.name like '%" +
                        foodName + "%' and F.resId = R.id group by id limit " + from + " , " + offset + " ;");
        while(result.next()) {
            RestaurantDAO restaurantDAO = new RestaurantDAO();
            restaurantDAO.setId(result.getString("id"));
            restaurantDAO.setName(result.getString("name"));
            Location resLoc = new Location();
            resLoc.setX(result.getInt("locx"));
            resLoc.setY(result.getInt("locy"));
            restaurantDAO.setLocation(resLoc);
            restaurantDAO.setLogo(result.getString("logo"));
            restaurants.add(restaurantDAO);
        }
        result.close();
        statement.close();
        connection.close();
        return restaurants;
    }
}
