package org.fn.ie.repository;

import org.fn.ie.domain.Order;
import java.util.ArrayList;

public class FactorDAO {
    private String restaurantName;
    private ArrayList<Order> orders;
    private int netPrice;

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
        int net = 0;
        for(int i = 0; i < orders.size(); i++) {
            net += orders.get(i).getCount() * orders.get(i).getPrice();
        }
        netPrice = net;
    }

    public int getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(int netPrice) {
        this.netPrice = netPrice;
    }
}
